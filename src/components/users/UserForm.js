import React, {Fragment, useState} from 'react';
import PropTypes from 'prop-types';
import {connect} from 'react-redux';
import {addUser} from '../../actions/user';

import Button from 'react-bootstrap/Button';
import Modal from 'react-bootstrap/Modal';

import {AvField, AvForm} from "availity-reactstrap-validation"

const UserForm = ({ addPost, handleShow, handleClose, show }) => {

  const [text, setText] = useState('');

  const AddUser = (event, value) =>{
    addUser(value);
  };

  return (
    <Fragment>
      <Modal show={show} onHide={handleClose}>
        <Modal.Header closeButton>
          <Modal.Title>Yangi foydalanuvchi qo'shish</Modal.Title>
        </Modal.Header>
        <Modal.Body>

          <AvForm onValidSubmit={AddUser}>

            <AvField
                name="name"
                label="Name"
                type="text"
                required
            />

            <AvField
                name="email"
                label="Email"
                type="email"
                required
            />

            <AvField
                name="password"
                label="Password"
                type="password"
                required
            />

            <Button variant="primary" type="submit">Saqlash</Button>
          </AvForm>

        </Modal.Body>
        <Modal.Footer>
          <Button variant="secondary" onClick={handleClose}>
            Yopish
          </Button>
          <Button variant="primary" onClick={handleClose}>
            Saqlash
          </Button>
        </Modal.Footer>
      </Modal>
    </Fragment>
  );
};

UserForm.propTypes = {
  addUser: PropTypes.func.isRequired,
};

export default connect(null, { addUser})(UserForm);
